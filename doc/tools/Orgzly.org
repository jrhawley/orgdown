★ [[../Tool-Support.org][← Back to Tool Support]] ★ [[../Overview.org][↑ Overview page]] ★

* Orgdown support for Orgzly

- Platform: any
- [[http://www.orgzly.com/][Homepage]]
- License: [[https://github.com/orgzly/orgzly-android/blob/master/LICENSE][GNU General Public License version 3 (or newer)]]
- Price: free
- [[http://www.orgzly.com/help][Documentation]] 
- [[https://github.com/orgzly/orgzly-android][Source code]] and [[https://github.com/orgzly/orgzly-android/issues][issue management]]
- last Orgdown1 syntax assessment: 2021-08-12 with Orgzly v1.8.5 by [[https://Karl-Voit.at][Karl Voit]] 

| *Percentage of Orgdown1 syntax support:* | 86 |
#+TBLFM: @1$2=(remote(toolsupport,@>$2)/86)*100;%.0f

** Details

- legend:
  - *1* → works as simple text
  - *2* → at least one of:
    - syntax highlighting
    - support for inserting/collapse/expand/…
    - support for toggling (e.g., checkboxes, states, …)

#+NAME: toolsupport
| Element                                                                     | OD1 | OD1 comment |
|-----------------------------------------------------------------------------+-----+-------------|
| syntax elements do not need to be separated via empty lines                 |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| *bold*                                                                      |   2 |             |
| /italic/                                                                    |   2 |             |
| _underline_                                                                 |   2 |             |
| +strike through+                                                            |   2 |             |
| =code=                                                                      |   1 |             |
| ~commands~                                                                  |   1 |             |
| Combinations like *bold and /italic/ text*                                  |   2 |             |
| : example                                                                   |   1 |             |
| [[https://example.com][link *bold* test]]                                                            |   2 |             |
| [[https://example.com][link /italic/ test]]                                                          |   2 |             |
| [[https://example.com][link +strike through+ test]]                                                  |   2 |             |
| [[https://example.com][link =code= test]]                                                            |   1 |             |
| [[https://example.com][link ~commands~ test]]                                                        |   1 |             |
| ≥5 dashes = horizontal bar                                                  |   1 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Headings using asterisks                                                    |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Unordered list item with "-"                                                |   2 |             |
| Ordered list item with 1., 2., …                                            |   2 |             |
| Checkbox + unordered list item with "-"                                     |   2 |             |
| Checkbox + ordered list item with 1., 2., …                                 |   2 |             |
| Mixed lists of ordered and unordered items                                  |   2 |             |
| Multi-line list items with collapse/expand                                  |   2 |             |
| Multi-line list items with support for (auto-)indentation                   |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Example block                                                               |   1 |             |
| Quote block                                                                 |   1 |             |
| Verse block                                                                 |   1 |             |
| Src block                                                                   |   1 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Comment lines with "# <foobar>"                                             |   2 |             |
| Comment block                                                               |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| https://gitlab.com/publicvoit/orgdown plain URL without brackets            |   2 |             |
| [[https://gitlab.com/publicvoit/orgdown]] URL with brackets without description |   2 |             |
| [[https://gitlab.com/publicvoit/orgdown][OD homepage]] URL with brackets with description                              |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Basic table functionality                                                   |   2 |             |
| Table cells with *bold*                                                     |   2 |             |
| Table cells with /italic/                                                   |   2 |             |
| Table cells with _underline_                                                |   2 |             |
| Table cells with +strike through+                                           |   2 |             |
| Table cells with =code=                                                     |   2 |             |
| Table cells with ~commands~                                                 |   2 |             |
| Auto left-aligning of text                                                  |   1 |             |
| Auto right-aligning of numbers like "42.23"                                 |   1 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Other syntax elements are interpreted at least as normal text (or better)   |   2 |             |
| Other syntax elements: linebreaks respected (or better)                     |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| *Sum*                                                                       |  74 |             |
#+TBLFM: @>$2=vsum(@I$2..@>>$2)


